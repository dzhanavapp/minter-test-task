export default function(data) {
  let validators = [];
  data.forEach((coin) => {
    let i = validators.findIndex(
      (x) => x.public_key == coin.validator.public_key
    );
    if (i <= -1) {
      validators.push(coin.validator);
    }
  });

  let result = validators.map((validator) => {
    let validatorWithCoins = Object.assign({ ...validator, coins: [] });
    data.forEach((coin) => {
      if (coin.validator.public_key == validator.public_key) {
        validatorWithCoins.coins.push(coin);
      }
    });

    return validatorWithCoins;
  });
  return result;
}
