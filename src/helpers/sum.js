export default function(array, prop) {
  var total = 0;
  for (var i = 0; i < array.length; i++) {
    total += array[i][prop];
  }
  return total;
}
