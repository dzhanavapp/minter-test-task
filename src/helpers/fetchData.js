import formatJson from "./formatJson";

export default async function fetchData(url) {
  return await fetch(url)
    .then((response) => {
      if (response.ok) {
        return response.json();
      } else {
        throw new Error("Ошибка запроса");
      }
    })
    .then((responseJson) => {
      return [
        formatJson(responseJson.data),
        responseJson.meta.additional.total_delegated_bip_value,
      ];
    })
    .catch(() => {
      return null;
    });
}
